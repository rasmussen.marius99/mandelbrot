// Copyright (C) 2021 Marius Rasmussen, David Truby
//
// This file is part of mandelbrot.
//
// mandelbrot is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// mandelbrot is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with mandelbrot; if not, write to the Free Software
// Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

use wgpu::util::{BufferInitDescriptor, DeviceExt};
use winit::{
    event::*,
    event_loop::*,
    window::{Window, WindowBuilder},
};

use glam::Vec2;

use bytemuck::{Pod, Zeroable};

#[derive(Copy, Clone, Pod, Zeroable, Debug)]
#[repr(C)]
struct Coords {
    x_min: f32,
    x_max: f32,
    y_min: f32,
    y_max: f32,
}

#[derive(Copy, Clone, Pod, Zeroable, Debug)]
#[repr(C)]
struct FrameConstants {
    width: u32,
    height: u32,
    zoom: f32,
    mouse_x: f32,
    mouse_y: f32,
    x_min: f32,
    x_max: f32,
    y_min: f32,
    y_max: f32,
}
struct WindowState {
    surface: wgpu::Surface,
    device: wgpu::Device,
    queue: wgpu::Queue,
    size: winit::dpi::PhysicalSize<u32>,
    pipeline: wgpu::RenderPipeline,
    constants: FrameConstants,
    constant_buffer: wgpu::Buffer,
    constant_bind_group: wgpu::BindGroup,
}

impl WindowState {
    async fn new(window: &Window) -> WindowState {
        let size = window.inner_size();

        let instance = wgpu::Instance::new(wgpu::Backends::PRIMARY);
        let surface = unsafe { instance.create_surface(window) };
        let adapter = instance
            .request_adapter(&wgpu::RequestAdapterOptions {
                power_preference: wgpu::PowerPreference::HighPerformance,
                compatible_surface: Some(&surface),
            })
            .await
            .unwrap();

        let (device, queue) = adapter
            .request_device(
                &wgpu::DeviceDescriptor {
                    features: wgpu::Features::default(),
                    limits: wgpu::Limits::default(),
                    label: None,
                },
                None,
            )
            .await
            .unwrap();

        let constants = FrameConstants {
            width: size.width,
            height: size.height,
            zoom: 1.0,
            mouse_x: 0.0,
            mouse_y: 0.0,
            x_min: -2.0,
            x_max: 2.0,
            y_min: -2.0,
            y_max: 2.0,
        };

        let constant_buffer = device.create_buffer_init(&wgpu::util::BufferInitDescriptor {
            label: Some("Constant Buffer"),
            contents: bytemuck::cast_slice(&[constants]),
            usage: wgpu::BufferUsages::UNIFORM | wgpu::BufferUsages::COPY_DST,
        });

        let constant_size = std::mem::size_of::<FrameConstants>() as wgpu::BufferAddress;
        let constant_bind_group_layout =
            device.create_bind_group_layout(&wgpu::BindGroupLayoutDescriptor {
                label: Some("Constant Bind Group Layout"),
                entries: &[wgpu::BindGroupLayoutEntry {
                    binding: 0,
                    visibility: wgpu::ShaderStages::all(),
                    ty: wgpu::BindingType::Buffer {
                        ty: wgpu::BufferBindingType::Uniform,
                        has_dynamic_offset: false,
                        min_binding_size: wgpu::BufferSize::new(constant_size),
                    },
                    count: None,
                }],
            });

        let constant_bind_group = device.create_bind_group(&wgpu::BindGroupDescriptor {
            label: Some("Constant Bind Group"),
            layout: &constant_bind_group_layout,
            entries: &[wgpu::BindGroupEntry {
                binding: 0,
                resource: constant_buffer.as_entire_binding(),
            }],
        });

        // let coord_buffer = device.create_buffer_init(&wgpu::util::BufferInitDescriptor {
        //     label: Some("Coordinate Buffer"),
        //     contents: bytemuck::cast_slice(&[coords]),
        //     usage: wgpu::BufferUsages::STORAGE
        // });
        // let coord_size = std::mem::size_of_val(&coords) as wgpu::BufferAddress;
        // let coord_bind_group_layout = device.create_bind_group_layout(&wgpu::BindGroupLayoutDescriptor {
        //     label: Some("Coord Bind Group Layout"),
        //     entries: &[
        //         wgpu::BindGroupLayoutEntry {
        //             binding: 0,
        //             visibility: wgpu::ShaderStages::FRAGMENT,
        //             ty: wgpu::BindingType::Buffer {
        //                 ty: wgpu::BufferBindingType::Storage{read_only: false},
        //                 has_dynamic_offset: false,
        //                 min_binding_size: wgpu::BufferSize::new(coord_size)
        //             },
        //             count: None
        //         }
        //     ]
        // });

        // let coord_bind_group = device.create_bind_group(&wgpu::BindGroupDescriptor {
        //     label: Some("Coord Bind Group"),
        //     layout: &coord_bind_group_layout,
        //     entries: &[
        //         wgpu::BindGroupEntry {
        //             binding: 0,
        //             resource: coord_buffer.as_entire_binding()
        //         }
        //     ]
        // });

        let source = wgpu::include_wgsl!("shader.wgsl");

        let shader = device.create_shader_module(&source.into());

        let pipeline_layout = device.create_pipeline_layout(&wgpu::PipelineLayoutDescriptor {
            label: None,
            bind_group_layouts: &[&constant_bind_group_layout],
            push_constant_ranges: &[],
        });

        let pipeline = device.create_render_pipeline(&wgpu::RenderPipelineDescriptor {
            label: None,
            layout: Some(&pipeline_layout),
            vertex: wgpu::VertexState {
                module: &shader,
                entry_point: "vert_main",
                buffers: &[],
            },
            primitive: wgpu::PrimitiveState::default(),
            depth_stencil: None,
            multisample: wgpu::MultisampleState::default(),
            fragment: Some(wgpu::FragmentState {
                module: &shader,
                entry_point: "main",
                targets: &[wgpu::ColorTargetState {
                    format: surface.get_preferred_format(&adapter).unwrap(),
                    blend: Some(wgpu::BlendState::REPLACE),
                    write_mask: wgpu::ColorWrites::ALL,
                }],
            }),
        });

        let config = wgpu::SurfaceConfiguration {
            usage: wgpu::TextureUsages::RENDER_ATTACHMENT,
            format: surface.get_preferred_format(&adapter).unwrap(),
            width: size.width,
            height: size.height,
            present_mode: wgpu::PresentMode::Immediate,
        };

        surface.configure(&device, &config);

        Self {
            surface,
            device,
            queue,
            size,
            pipeline,
            constants,
            constant_buffer,
            constant_bind_group,
        }
    }

    fn resize(&mut self, new_size: winit::dpi::PhysicalSize<u32>) {
        self.constants.width = new_size.width;
        self.constants.height = new_size.height;
    }

    fn scroll(&mut self, y: f64) {
        // if y > 0.0 {
        //     self.constants.zoom = self.constants.zoom + 0.2;
        // } else {
        //     self.constants.zoom = self.constants.zoom - 0.2;
        // }
        let mut constants = &mut self.constants;
        let mut mouse_x = constants.mouse_x / (constants.width as f32 / 2.0) - 1.0;
        let mut mouse_y = constants.mouse_y / (constants.width as f32 / 2.0) - 1.0;
        mouse_x = (mouse_x + 1.0) * 0.5;
        mouse_y = (mouse_y + 1.0) * 0.5;
        let x_diff = constants.x_max - constants.x_min;
        let y_diff = constants.y_max - constants.y_min;
        constants.mouse_x = constants.x_min + (x_diff * mouse_x);
        constants.mouse_y = constants.y_min + (y_diff * mouse_y);

        println!("x: {} y: {}", constants.mouse_x, constants.mouse_y);

        let (new_xdiff, new_ydiff) = if y > 0.0 {
            ((x_diff * 0.9) * 0.5, (y_diff * 0.9) * 0.5)
        } else {
            ((x_diff / 0.9) * 0.5, (y_diff / 0.9) * 0.5)
        };

        constants.x_min = constants.mouse_x - new_xdiff * 0.5;
        constants.y_min = constants.mouse_y - new_ydiff * 0.5;
        constants.x_max = constants.mouse_x + new_xdiff * 0.5;
        constants.y_max = constants.mouse_y + new_ydiff * 0.5;
    }

    fn input(&mut self, event: &WindowEvent) -> bool {
        use winit::event::MouseScrollDelta::*;
        match event {
            WindowEvent::MouseWheel { delta, .. } => {
                match delta {
                    LineDelta(_, y) => self.scroll(*y as f64),
                    PixelDelta(winit::dpi::PhysicalPosition { y, .. }) => self.scroll(*y),
                };
                true
            }
            WindowEvent::CursorMoved { position, .. } => {
                self.constants.mouse_x = position.x as f32;
                self.constants.mouse_y = position.y as f32;
                true
            }
            _ => false,
        }
    }

    fn update(&mut self) {}

    fn render(&mut self) -> Result<(), wgpu::SurfaceError> {
        let frame = self.surface.get_current_frame()?.output;

        let mut encoder = self
            .device
            .create_command_encoder(&wgpu::CommandEncoderDescriptor {
                label: Some("Render Encoder"),
            });

        {
            let view = frame
                .texture
                .create_view(&wgpu::TextureViewDescriptor::default());
            let constant_buffer =
                self.device
                    .create_buffer_init(&wgpu::util::BufferInitDescriptor {
                        label: Some("Constant Buffer"),
                        contents: bytemuck::cast_slice(&[self.constants]),
                        usage: wgpu::BufferUsages::UNIFORM | wgpu::BufferUsages::COPY_SRC,
                    });
            encoder.copy_buffer_to_buffer(
                &constant_buffer,
                0,
                &self.constant_buffer,
                0,
                std::mem::size_of_val(&self.constants) as wgpu::BufferAddress,
            );
            let mut render_pass = encoder.begin_render_pass(&wgpu::RenderPassDescriptor {
                label: Some("Render Pass"),
                color_attachments: &[wgpu::RenderPassColorAttachment {
                    view: &view,
                    resolve_target: None,
                    ops: wgpu::Operations {
                        load: wgpu::LoadOp::Clear(wgpu::Color {
                            r: 0.1,
                            g: 0.2,
                            b: 0.3,
                            a: 1.0,
                        }),
                        store: true,
                    },
                }],
                depth_stencil_attachment: None,
            });

            render_pass.set_pipeline(&self.pipeline);
            render_pass.set_bind_group(0, &self.constant_bind_group, &[]);
            render_pass.draw(0..3, 0..1);
        }

        self.queue.submit(std::iter::once(encoder.finish()));

        Ok(())
    }
}

fn main() {
    env_logger::init();
    let event_loop = EventLoop::new();
    let logical = winit::dpi::LogicalSize::new(1000, 1000);
    let window = WindowBuilder::new()
        .with_inner_size(logical)
        .build(&event_loop)
        .unwrap();

    let mut window_state = pollster::block_on(WindowState::new(&window));

    event_loop.run(move |elevent, _, control_flow| match elevent {
        Event::RedrawRequested(_) => {
            window_state.update();
            match window_state.render() {
                Ok(_) => {}
                Err(wgpu::SurfaceError::Lost) => window_state.resize(window_state.size),
                Err(wgpu::SurfaceError::OutOfMemory) => *control_flow = ControlFlow::Exit,
                Err(e) => eprintln!("{:?}", e),
            }
        }
        Event::MainEventsCleared => {
            window.request_redraw();
        }
        Event::WindowEvent {
            ref event,
            window_id,
        } if window_id == window.id() => {
            if !window_state.input(event) {
                match event {
                    WindowEvent::CloseRequested => *control_flow = ControlFlow::Exit,
                    _ => {}
                }
            }
        }
        _ => {}
    });
}
