[[block]] struct Complex {
  real : f32;
  imag : f32;
};

struct VertexOutput {
  [[builtin(position)]] Position : vec4<f32>;
  [[location(0)]] fragCoord : vec2<f32>;
};

[[block]] struct Coords {
  x_min: f32;
  x_max: f32;
  y_min: f32;
  y_max: f32;
};

[[block]] struct FrameConstants {
  width : u32;
  height : u32;
  zoom : f32;
  mouse_x: f32;
  mouse_y: f32;
  x_min: f32;
  x_max: f32;
  y_min: f32;
  y_max: f32;
};

[[group(0), binding(0)]]
var<uniform> constants : FrameConstants;

// [[group(1), binding(0)]]
// var coords : Coords;

let ITERATIONS : i32 = 1000;

fn complex_new(real: f32, imag: f32) -> Complex {
  var out : Complex;
  out.real = real;
  out.imag = imag;
  return out;
}

fn mult(z1 : Complex, z2 : Complex) -> Complex {
  var out : Complex;
  out.real = z1.real * z2.real - z1.imag * z2.imag;
  out.imag = z1.real * z2.imag + z1.imag * z2.real;
  return out;
}

fn norm(z : Complex) -> f32 {
  return z.real * z.real + z.imag * z.imag;
}

fn iteration(c : Complex, z : Complex) -> Complex {
   var m = mult(z,z);
   var out : Complex;
   out.real = m.real + c.real;
   out.imag = m.imag + c.imag;
   return out;
}

fn translate_to_grid(coord: vec2<f32>) -> Complex {
  let xdiff = constants.x_max - constants.x_min;
  let ydiff = constants.y_max - constants.y_min;
  var out : Complex;
  out.real = ((coord.x + 1.0) * 0.5) * xdiff + constants.x_min;
  out.imag = ((coord.y + 1.0) * 0.5) * ydiff + constants.y_min;
  return out;
}

fn calc_pixel(p: vec2<f32>) -> vec4<f32> {
  var z : Complex = complex_new(0.,0.);
  var c : Complex = translate_to_grid(p);

  for(var j = 0; j < ITERATIONS; j=j+1) {
    z = iteration(c,z);
    if (norm(z) > 4.) {
      return vec4<f32>(0.,0.,0.,1.);
    }
  }

  return vec4<f32>(1.,1.,1.,1.);
}

[[stage(fragment)]] fn main(in: VertexOutput) -> [[location(0)]] vec4<f32> {
  return calc_pixel(in.fragCoord);
}

[[stage(vertex)]]
fn vert_main([[builtin(vertex_index)]] VertexIndex : u32) -> VertexOutput {
  var out : VertexOutput;

  let x2 = VertexIndex << 1u;
  let uv = vec2<f32>(f32(x2 & 2u), f32(VertexIndex & 2u));
  let pos = 2. * uv - vec2<f32>(1.,1.);

  out.Position = vec4<f32>(pos, 0., 1.);
  out.fragCoord = pos;
  return out;
}
